# Backout 2.1

BackOut is a typeface originally created by [Frank Adebiaye](http://www.fadebiaye.com/) in 2012.
BackOut 2.0 (code name: Abomey) was created by [Ariel Martín Pérez](http://arielgraphisme.com/) during the very hot summer of 2019. Both versions have been released by the [Velvetyne Type Foundry](http://velvetyne.fr/fonts/backout).

![specimen1](documentation/specimen/specimen-backout-02.jpg)

Inspired by humanist typefaces such as Albertus, BackOut has a decidely African design. Its name comes from the eponymous song by Bob Marley and The Wailers.

The 2.0 version of BackOut is a faithful review of Frank's original with improved spacing and kerning, corrected contrast, new lowercase letters and extended language support and functionalities. In October 2022, the 2.1 version was released, with revised kerning and some minor modifications.

Contribute or download it on [Velvetyne Type Foundry](http://velvetyne.fr/fonts/backout/).

## Specimen

![specimen1](documentation/specimen/specimen-backout-03.jpg)
![specimen1](documentation/specimen/specimen-backout-04.jpg)

## License

BackOut is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/raphaelbastide/Unified-Font-Repository
